const express = require('express');
const app = express();
const path = require('path');
const mongoose = require('mongoose');
const pug = require('pug');
const cookieParser = require('cookie-parser');
const PORT = process.env.PORT || 3000;
const bodyParser = require('body-parser');
const flash = require('connect-flash');
const session = require('express-session');
const passport = require('passport');


// router variables
const homeRouter = require('./routes/home');
const articleRouter = require('./routes/article');
const registerRouter = require('./routes/register');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Set static folder
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'scripts')));

//Middleware session
app.use(session({
    secret: 'keyboard cat',
    resave: true,
    saveUninitialized: true,
}));

//Middleware message
app.use(require('connect-flash')());
app.use(function (req, res, next) {
  res.locals.messages = require('express-messages')(req, res);
  next();
});

require('./config/passport')(passport);
app.use(cookieParser());
app.use(session({ secret: 'session secret key' }));
app.use(passport.initialize());
app.use(passport.session());

app.get('*', function(req,res,next){
    res.locals.user = req.user || null;
    next();
});

//Load view engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// Load routers
app.use('/', homeRouter);
app.use('/api/articles', articleRouter);
app.use('/', registerRouter);


app.listen('3000', () => {
    console.log(`App listening on ${PORT} port`);
});