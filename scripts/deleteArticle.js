$( ".delete" ).click(function() {
    let conf = confirm("Are you sure want delete article ?");

    if(conf) {
        let id = $(this).attr('data-id');
        $.ajax({
            url         : "/api/articles/delete/" + id, 
            method      : "DELETE", 
            success: function(resp) {
                window.location.href = '/api/articles';
            }, 
            error: function(err) {
                console.log(err);
            }
        });
    }
});