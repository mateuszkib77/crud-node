const express = require('express');
const router = express.Router();
const db = require('../database/db');
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const { check, validationResult } = require('express-validator/check');
const User = require('../models/user');
const passport = require('passport');

router.get('/register', (req,res) => {
    res.render('auth/register');
});

router.post('/register', [
        check('name').isLength({min: 3}).withMessage("Field name must have minimum 3 character"),
        check('email').isEmail().withMessage("Incorrect format email").not().isEmpty().withMessage("Field email is required!"),
        check('password').not().isEmpty().withMessage("Field password is required!"),
        check('password-confirmation').not().isEmpty().withMessage("Please confirm your password!").isLength({min: 5}).withMessage("Field password must have minimum 5 character")
        .custom((value,{req, loc, path}) => {
            if (value !== req.body.password) {
            } else {
                return value;
            }
        }).withMessage("Passwords don't match")
    ],
    (req,res) => {
    
    const errors = validationResult(req);
    
    async function checkIfEmailExist() {
        return await User.find()
        .or([ {'name': req.body.name}, {'email': req.body.email} ]);
    }
    
    checkIfEmailExist().then(arr => {
        let existEmail, existName, myValidate;

        if( !(arr === undefined || arr.length == 0) ){
            myValidate = true;
            existName = (arr[0].name == req.body.name) ? true : false; 
            existEmail = (arr[0].email == req.body.email) ? true : false;
        }

        if( !( errors.isEmpty()) || myValidate ) {
            res.render('auth/register', {
                errors: errors.mapped(),
                existEmail: existEmail ? "This email exist!" : "",
                existName: existName ? "This name exist!" : ""
            });
        } else {
            let salt = bcrypt.genSaltSync(10);
            let hashPassword = bcrypt.hashSync(req.body.password, salt);
    
            let user = new User({
                name: req.body.name,
                email: req.body.email,
                password: hashPassword,
            });
            
            async function saveUser(){
                await user.save((err) => {
                    if(err) {
                        console.log(err);
                        return;
                    }
                    req.flash("success", "The registration was successful");
                    res.redirect('/api/articles');
                })
            }
            saveUser();
        }
    });
});

router.get('/login', (req,res) => {
    res.render('auth/login');
});

router.post('/login', function(req,res,next){
  passport.authenticate('local', { successRedirect: '/api/articles',
                                   failureRedirect: '/login',
                                   failureFlash: true })(req,res,next)
});

router.get('/logout', (req,res) => {
    req.logout();
    req.flash('success', 'You are logged out');
    res.redirect('/api/articles');
});

module.exports = router;