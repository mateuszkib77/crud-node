const express = require('express');
const router = express.Router();
const db = require('../database/db');
const mongoose = require('mongoose');
const { check, validationResult } = require('express-validator/check');
const Article = require('../models/article');


router.get('/', (req,res) => {
    Article.find({},(err,articles)=> {
        res.render('article/show_article', {title: "All articles", articles: articles});
    })
});

router.post('/', 
    [
        check('title').isLength({min: 3}).withMessage('Title must have minimum 3 characters!'),
        check('body').isLength({min: 1}).withMessage('Field body is a required!')
    ], (req,res) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.render('article/add_article', {
          errors: errors.mapped()
      });
    } else {
    let article = new Article({
        title: req.body.title,
        author: req.user.name,
        body: req.body.body
    });
 
    async function saveArticle(){
        await article.save((err) => {
            if(err){
                console.log(err);
                return;
            } else {
                req.flash("success", "Article was add successfully");
                res.redirect('/api/articles');
            }
        });
    }
    
    saveArticle();
}
});

router.get('/add', isAuth, (req,res) => {
    let title = "Add article";
    res.render('article/add_article', {title : title});
});

router.get('/:id', (req,res) => {
    getSingleArticleById(req.params.id)
    .then(article => {
        res.render('article/single_article', {article: article});
    })
    .catch(err => console.error(err));
});

router.get('/edit/:id', isAuth, (req,res) => {
    getSingleArticleById(req.params.id)
    .then(article => {
        if(article.author == req.user.name){
            res.render('article/edit_article', {article: article});
        } else {
            req.flash('danger', "Not authorized");
            res.redirect('/api/articles');
        }
    })
    .catch(err => console.error(err));
});

router.post('/edit/:id', (req,res) => {
    async function updateArticle(){
        await Article.update({_id: req.params.id}, {
            $set : {
                title: req.body.title,
                author: req.user.name,
                body: req.body.body
            }
        });
    }
    updateArticle();
    res.redirect('/api/articles');
});

router.delete('/delete/:id', (req,res) => {
    async function deleteArticle() {
        await Article.deleteOne({_id: req.params.id}, (err) => {
            if(err) {
                console.log(err);
                return;
            }
        });
    }
    deleteArticle();
    res.send('Success');
});


// Functions
async function getSingleArticleById(id){
    return await Article.findById(id,(err) => {
        if(err) {
            console.log(err);
            return;
        }
    }); 
}

function isAuth(req,res,next){
    if(req.isAuthenticated()){
        return next();
    } else {
        req.flash('danger','Please login');
        res.redirect('/login');
    }
};



module.exports = router;