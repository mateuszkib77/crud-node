let mongoose = require('mongoose');

let db = mongoose.connect("mongodb://localhost/crud-node")
.then(() => console.log("Connected to MongoDB"))
.catch((err) => console.log("Error " + err));

module.exports = db;