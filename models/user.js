const mongoose = require('mongoose');

let userSchema = mongoose.Schema({
    name: String,
    email: String,
    password: String,
    crDate: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('User', userSchema);