let mongoose = require('mongoose');

let articleSchema = mongoose.Schema({
    title: {
        type: String
    }, 
    author: {
        type: String
    },
    body: {
        type: String
    },
    date: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Article', articleSchema);